<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Companies extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'comment',
    ];

    public function services()
    {
        // return $this->belongsToMany(Services::class);
        return $this->belongsToMany(Services::class, 'companies_services', 'company_id', 'service_id');
    }
}
