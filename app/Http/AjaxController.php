<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Companies;
use App\Services;

class AjaxController extends Controller
{
    /**
     * get all company's services
     *
     * @return json
     */
    public function edit()
    {
        $validated = request()->validate([
            'owner_id' => 'required|integer',
        ]);

        $company = Companies::findOrFail($validated['owner_id']);

        return [
            'success' => true,
            'data' => $company->services()->get()->toArray(),
            'columns' => Services::columns(),
        ];
    }

    /**
     * save company's services
     *
     * @return json
     */
    public function save()
    {
        $validated = request()->validate([
            'owner_id' => 'required|integer',
            'data' => 'required|json',
        ]);

        $company = Companies::findOrFail($validated['owner_id']);

        $data = collect(array_filter(array_map('array_filter', json_decode($validated['data'], true))))->unique('title');

        try {
            $services = collect();
            foreach ($data as $service_data) {
                if (array_key_exists('title', $service_data)) {
                    $service = Services::where('title', $service_data['title'])->first();
                    if (!$service) {
                        $service = Services::create(['title' => $service_data['title']]);
                    }
                    $service->update([
                        'cost' => array_key_exists('cost', $service_data) ? $service_data['cost'] : null,
                        'photo' => array_key_exists('photo', $service_data) ? $service_data['photo'] : null,
                    ]);

                    $services->push($service);
                    $services = $services->reverse()->unique('title');
                }
            }

            $company->services()->sync($services->pluck('id'));

            return ['success' => true];
        } catch (Exception $e) {
            // TODO: errors handling
        }

        return ['success' => false];
    }

    /**
     * upload photo and return it's url
     *
     * @return json
     */
    public function uploadPhoto()
    {
        // TODO: implement method
        return [
            'success' => true,
            'photo' => 'http://127.0.0.1:8000/images/googlelogo_color_272x92dp.png'
        ];
    }
}
