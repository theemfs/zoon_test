<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Services extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'cost',
        'comment',
        'photo',
        // 'photo_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be visible in serialization.
     *
     * @var array
     */
    protected $visible = [
        'title', 'cost', 'id', 'photo'
    ];

    public function photo()
    {
        return $this->hasOne(Photos::class, 'id', 'photo_id');
    }

    public static function columns()
    {
        $columns = [
            'title' => [
                'title' => 'Услуга',
                'placeholder' => 'Услуга',
                'description' => 'Введите название услуги',
            ],
            'cost' => [
                'title' => 'Цена, руб',
                'placeholder' => 'Цена, руб',
                'description' => 'Введите одно число',
            ],
            'photo' => [
                'title' => 'Фото',
                'placeholder' => 'Фото',
                'description' => 'Вставьте ссылку на фото или загрузите с компьютера. Можно загрузить только одно фото.',
            ],
        ];

        return $columns;
    }
}
