<?php

Route::get('/', function () {
    return view('index');
});

Route::match(['get', 'post'], '/ajax.php', function () {
    // return request()->all();

    switch ($_REQUEST['action']) {
    case 'edit':
        //todo: часть 1. загрузка меню
        $res = app('App\Http\Controllers\AjaxController')->edit();
        break;

    case 'save':
        //todo: часть 1. сохранение меню
        $res = app('App\Http\Controllers\AjaxController')->save();
        break;

    case 'uploadPhoto':
        //todo: часть 2. аплоадим файл, возвращаем ссылку на него
        $res = app('App\Http\Controllers\AjaxController')->uploadPhoto();
        break;
    default:
        $res = ['success' => false];
    }

    return $res;
});
