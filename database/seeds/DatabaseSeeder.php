<?php

use Illuminate\Database\Seeder;
use App\Services;
// use App\Photos;
use App\Companies;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        // $this->call(PhotosTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(CompaniesServicesTableSeeder::class);
    }
}

class ServicesTableSeeder extends Seeder
{
    public function run()
    {
        $service = new Services();
        $service->title = 'Гороховый суп';
        $service->cost = 120;
        // $service->photo_id  = 1;
        $service->photo  = 'http://127.0.0.1:8000/images/zoon_logo.png';
        $service->save();

        $service = new Services();
        $service->title = 'Борщ';
        $service->cost = 140;
        // $service->photo_id  = 1;
        $service->photo  = 'http://127.0.0.1:8000/images/zoon_logo.png';
        $service->save();

        $service = new Services();
        $service->title = 'Котлета';
        $service->cost = 130;
        // $service->photo_id  = 1;
        $service->photo  = 'http://127.0.0.1:8000/images/zoon_logo.png';
        $service->save();

        $service = new Services();
        $service->title = 'Компот';
        $service->cost = 30;
        // $service->photo_id  = 1;
        $service->photo  = 'http://127.0.0.1:8000/images/zoon_logo.png';
        $service->save();
    }
}

class PhotosTableSeeder extends Seeder
{
    public function run()
    {
        $photo = new Photos();
        $photo->name = 'zoon_logo.png';
        $photo->type = 'png';
        $photo->path = 'images/3f719adcd422d2fd4316baf2fb87928a';
        $photo->url = 'http://127.0.0.1:8000/images/zoon_logo.png';
        $photo->save();
    }
}

class CompaniesTableSeeder extends Seeder
{
    public function run()
    {
        $company = new Companies();
        $company->name = 'First Company';
        $company->save();
    }
}

class CompaniesServicesTableSeeder extends Seeder
{
    public function run()
    {
        $services = Services::all();
        $company = Companies::find(1);
        $company->services()->sync($services);
    }
}
