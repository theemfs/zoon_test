<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->nullableTimestamps();
            $table->softDeletes();
            $table->string('comment')->nullable();

            $table->string('title')->nullable()->index();
            $table->integer('cost')->index()->nullable();
            $table->string('photo', 1024)->nullable();
            // $table->integer('photo_id')->index()->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
